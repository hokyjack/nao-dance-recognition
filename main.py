NAO_IP = "192.168.0.105"

from scipy.io.wavfile import write
from optparse import OptionParser
import naoqi
import numpy as np
import time
import sys
import librosa
import numpy as np
import random
from keras.models import model_from_json
import os
from sklearn.externals import joblib 

from naoqi import ALProxy

mapping = {
    0: 'cha_cha',
    1: 'jive',
    2: 'paso_doble',
    3: 'quickstep',
    4: 'rumba',
    5: 'samba',
    6: 'slowfox',
    7: 'tango',
    8: 'viennese_waltz',
    9: 'waltz'
}

scaler = None
encoder = None
model = None

def get_features(y, sr):
    feature_list = []
    chroma_stft = librosa.feature.chroma_stft(y=y, sr=sr)
    rmse = librosa.feature.rms(y=y)
    spec_cent = librosa.feature.spectral_centroid(y=y, sr=sr)
    spec_bw = librosa.feature.spectral_bandwidth(y=y, sr=sr)
    rolloff = librosa.feature.spectral_rolloff(y=y, sr=sr)
    zcr = librosa.feature.zero_crossing_rate(y)
    mfcc = librosa.feature.mfcc(y=y, sr=sr)
    
    feature_list = [np.mean(chroma_stft), np.mean(rmse), np.mean(spec_cent),
                    np.mean(spec_bw), np.mean(rolloff), np.mean(zcr)]
    for e in mfcc:
        feature_list.append(np.mean(e))
    return np.array(feature_list)


def predict_genre(songname, offset=0):
    global encoder
    y, sr = librosa.load(songname, mono=True, offset=offset, duration=5)
    
    norm_data = librosa.util.normalize(y)
    
    f = get_features(norm_data, sr).reshape(1, -1)
    X = scaler.transform(f)

    p = [(le_name_mapping[i],score) for (i,score) in enumerate(model.predict(X)[0])]
    for k,v in sorted(p, key=operator.itemgetter(1), reverse=True):
        print(k, "->", v)
    
    predictions = model.predict_classes(X)
    return mapping[predictions[0]].replace("_", " ")

class SoundReceiverModule(naoqi.ALModule):
    def __init__( self, strModuleName, strNaoIp ):
        try:
            naoqi.ALModule.__init__(self, strModuleName)
            self.BIND_PYTHON (self.getName(), "callback")
            self.strNaoIp = strNaoIp
        except BaseException, err:
            print("ERR: SoundReceiverModule: loading error: %s" % str(err))

    def __del__(self):
        self.stop()

    def start( self ):
        audio = naoqi.ALProxy("ALAudioDevice", self.strNaoIp, 9559)
        nNbrChannelFlag = 0
        nDeinterleave = 0
        nSampleRate = 48000
        audio.setClientPreferences(self.getName(),  nSampleRate, nNbrChannelFlag, nDeinterleave); # setting same as default generate a bug !?!
        audio.subscribe(self.getName())
        print ("INF: SoundReceiver: started!" )
        # self.processRemote( 4, 128, [18,0], "A"*128*4*2 ); # for local test
        # on romeo, here's the current order:
        # 0: right;  1: rear;   2: left;   3: front,  

    def stop (self):
        print("INF: SoundReceiver: stopping...")
        audio = naoqi.ALProxy("ALAudioDevice", self.strNaoIp, 9559)
        audio.unsubscribe(self.getName());        
        print("INF: SoundReceiver: stopped!")
        if (self.outfile != None): self.outfile.close()


    def processRemote(self, nbOfChannels, nbrOfSamplesByChannel, aTimeStamp, buffer ):
        """
        This is THE method that receives all the sound buffers from the "ALAudioDevice" module
        """
        aSoundDataInterlaced = np.fromstring(str(buffer), dtype=np.int16)
        aSoundData = np.reshape(aSoundDataInterlaced, (nbOfChannels, nbrOfSamplesByChannel), 'F')

        if hasattr(self, "sound"):
            self.sound = np.concatenate((self.sound, aSoundData), axis=1)
        else:
            self.sound = aSoundData
        

    def version (self):
        return "0.6"


def main ():
    global scaler
    global model
    global encoder

    parser = OptionParser()
    parser.add_option("--ip",
        help="Parent broker port. The IP address or your robot",
        dest="ip")
    parser.add_option("--port",
        help="Parent broker port. The port NAOqi is listening to",
        dest="port",
        type="int")
    parser.set_defaults(ip=NAO_IP, port=9559)
    opts, args_ = parser.parse_args()
    ip   = opts.ip
    port = opts.port

    myBroker = naoqi.ALBroker("myBroker", "0.0.0.0", 0, ip, port)    

     # initialization of ALProxy - TextToSpeech
    tts = ALProxy("ALTextToSpeech", ip, port)
    tts.setLanguage("English")
    tts.setParameter("speed", 70)

    # load json and create model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights("weights_3.h5")
    print("Loaded model from disk")

    scaler = joblib.load('scaler.pkl') 
    encoder = joblib.load('encoder.pkl') 

    # tts.say("Listening.")

    # Warning: SoundReceiver must be a global variable
    # The name given to the constructor must be the name of the variable
    global SoundReceiver
    SoundReceiver = SoundReceiverModule("SoundReceiver", ip)
    SoundReceiver.start()

    end = False
    start_time = time.time()

    try:
        while True:
            if time.time() - start_time > 6.5: # end after approx 5 seconds
                print("10 seconds passed, shutting down")
                end = True
                break

            time.sleep(1)
    except KeyboardInterrupt:
        print("Interrupted by user, shutting down")
        end = True
        
    if end:
        # save front microphone only
        write('./test_front.wav', 44100, SoundReceiver.sound[2])
        time.sleep(1)
        genre = predict_genre('./test_front.wav')
        print("This is a " + genre)
        tts.say("This is a " + genre)
        myBroker.shutdown()

        sys.exit(0)

if __name__ == "__main__":
    main()